package api

import (
	"example/user/hello/controller"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Register(ctx *gin.Context) {
	err := controller.Register(ctx)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Successfully saved users."})
}

// CreateCandidates godoc
// @Security bearerAuth
// @Summary Create new candidates
// @Description Create a new candidate
// @Tags candidates,create
// @Accept  json
// @Produce  json
// @Param candidates body entity.Candidate true "Create candidate"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /candidates [post]
func Login(ctx *gin.Context) {
	jwt, err := controller.Login(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"jwt": jwt})
	}
}
