package api

import (
	"net/http"

	"example/user/hello/controller"

	"github.com/gin-gonic/gin"
)

// Paths Information

// GetCandidates godoc
// @Security bearerAuth
// @Summary List existing candidates
// @Description Get all the existing candidates
// @Tags candidate,list
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.candidate
// @Failure 500 {object} dto.Response
// @Router /candidates [get]
func GetAllApplications(ctx *gin.Context) {
	resp, err := controller.GetAllApplications(ctx)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(200, resp)
}

// CreateCandidates godoc
// @Security bearerAuth
// @Summary Create new candidates
// @Description Create a new candidate
// @Tags candidates,create
// @Accept  json
// @Produce  json
// @Param candidates body entity.Candidate true "Create candidate"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /candidates [post]
func CreateApplication(ctx *gin.Context) {
	err := controller.AddApplication(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Successfully created application!",
		})
	}
}

// UpdateCandidate godoc
// @Security bearerAuth
// @Summary Update candidates
// @Description Update a single candidate
// @Security bearerAuth
// @Tags candidates
// @Accept  json
// @Produce  json
// @Param  id path int true "Candidate ID"
// @Param video body entity.Candidate true "Update candidate"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /candidate/{id} [put]
func UpdateApplication(ctx *gin.Context) {
	err := controller.UpdateApplication(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Successfully update applications!",
		})
	}
}

// DeleteCandidate godoc
// @Security bearerAuth
// @Summary Remove candidates
// @Description Delete a single candidate
// @Security bearerAuth
// @Tags candidates
// @Accept  json
// @Produce  json
// @Param  id path int true "Candidate ID"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /candidate/{id} [delete]
func DeleteApplication(ctx *gin.Context) {
	err := controller.DeleteApplication(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Success deleted applications!",
		})
	}
}

// GetCandidates godoc
// @Security bearerAuth
// @Summary List existing candidates
// @Description Get all the existing candidates
// @Tags candidate,list
// @Accept  json
// @Produce  json
// @Success 200 {array} entity.candidate
// @Failure 500 {object} dto.Response
// @Router /candidates [get]
func GetApplicationById(ctx *gin.Context) {
	resp, err := controller.GetApplicationById(ctx)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(200, resp)
}
