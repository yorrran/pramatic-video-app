package main

import (
	"example/user/hello/api"
	model "example/user/hello/entity"
	database "example/user/hello/repository"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {
	loadEnv()
	loadDatabase()
	serveApplication()
}

func loadDatabase() {
	database.Connect()

	database.Database.AutoMigrate(&model.User{})
	database.Database.AutoMigrate(&model.Candidate{})
	database.Database.AutoMigrate(&model.Application{})
	database.Database.AutoMigrate(&model.Interview{})

}

func loadEnv() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func serveApplication() {
	setupLogOutput()
	router := gin.Default()

	publicRoutes := router.Group("/auth")
	publicRoutes.POST("/register", api.Register)
	publicRoutes.POST("/login", api.Login)

	protectedRoutes := router.Group("/api")
	// protectedRoutes.Use(middlewares.JWTAuthMiddleware())
	protectedRoutes.POST("/candidate", api.CreateCandidate)
	protectedRoutes.GET("/candidates", api.GetAllCandiates)
	protectedRoutes.GET("/candidates/:id", api.GetCandidateById)
	protectedRoutes.PUT("/candidate/:id", api.UpdateCandidate)
	protectedRoutes.DELETE("/candidate/:id", api.DeleteCandidate)

	protectedRoutes.POST("/application", api.CreateApplication)
	protectedRoutes.GET("/applications", api.GetAllApplications)
	protectedRoutes.GET("/applications/:id", api.GetApplicationById)
	protectedRoutes.PUT("/application/:id", api.UpdateApplication)
	protectedRoutes.DELETE("/application/:id", api.DeleteApplication)

	protectedRoutes.POST("/interview", api.CreateInterview)
	protectedRoutes.GET("/interviews", api.GetAllInterviews)
	protectedRoutes.GET("/interviews/:id", api.GetInterviewById)
	protectedRoutes.PUT("/interview/:id", api.UpdateInterview)
	protectedRoutes.DELETE("/interview/:id", api.DeleteInterview)

	protectedRoutes.GET("/videos", api.GetVoice)
	protectedRoutes.POST("/videos", api.CreateVoice)
	protectedRoutes.PUT("/videos/:id", api.UpdateVoice)
	protectedRoutes.DELETE("/videos/:id", api.DeleteVoice)

	port := os.Getenv("PORT")
	if port == "" {
		port = "3333"
	}

	router.Run(":" + port)
	fmt.Println("Server running on port " + port)
}

func setupLogOutput() {
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}
