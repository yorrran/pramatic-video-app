package controller

import (
	"example/user/hello/entity"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

var validate *validator.Validate

type VideoController interface {
	Save(ctx *gin.Context) error
	FindAll() []entity.Video
	Update(ctx *gin.Context) error
	Delete(ctx *gin.Context) error
	ShowAll(ctx *gin.Context)
}

func Save(ctx *gin.Context) error {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}
	// err = validate.Struct(&video)
	fmt.Println("video save:", &video)
	if err != nil {
		return err
	}
	_, err = entity.SaveVideo(&video)
	return nil

}
func Update(ctx *gin.Context) error {
	var video entity.UpdateVideoParam
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	video.ID = id

	// err = validate.Struct(video)
	if err != nil {
		return err
	}

	param := &entity.UpdateVideoParam{
		ID:          id,
		Description: video.Description,
		Title:       video.Title,
		URL:         video.URL,
	}

	entity.UpdateVideo(param)
	return nil
}

func Delete(ctx *gin.Context) error {
	var video entity.Video
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		return err
	}
	video.ID = id
	fmt.Println("id:", id)
	entity.DeleteVideo(id)
	return nil
}

func FindAll(ctx *gin.Context) (gin.H, error) {
	videos, err := entity.FindAllVideos()

	if err != nil {
		return nil, err
	}

	resp := gin.H{
		"title":  "Video Page",
		"videos": videos,
	}
	return resp, nil
}
