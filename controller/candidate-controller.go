package controller

import (
	"example/user/hello/entity"
	"example/user/hello/service"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddCandidate(ctx *gin.Context) error {
	var candidate entity.Candidate
	err := ctx.ShouldBindJSON(&candidate)
	if err != nil {
		log.Default()
		return err
	}
	errSave := service.SaveCandidate(&candidate)
	return errSave
}

func UpdateCandidate(ctx *gin.Context) error {
	var param entity.UpdateCandidateParam
	err := ctx.ShouldBindJSON(&param)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}

	updateCandidate := &entity.UpdateCandidateParam{
		Username: param.Username,
		Email:    param.Email,
		Phone:    param.Phone,
	}

	errUpdate := service.UpdateCandidate(id, updateCandidate)
	return errUpdate
}

func DeleteCandidate(ctx *gin.Context) error {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}
	errDelete := service.DeleteCandidate(id)
	return errDelete
}

func GetAllCandidates(ctx *gin.Context) ([]entity.Candidate, error) {
	candidates, err := service.FindAllCandidates()
	if err != nil {
		return nil, err
	}
	return candidates, nil
}

func GetCandidateById(ctx *gin.Context) (*entity.Application, error) {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	candidate, err := service.FindApplicationById(id)
	if err != nil {
		return nil, err
	}
	return candidate, nil
}
