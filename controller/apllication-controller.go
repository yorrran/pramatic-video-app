package controller

import (
	"example/user/hello/entity"
	"example/user/hello/service"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddApplication(ctx *gin.Context) error {
	var application entity.Application
	err := ctx.ShouldBindJSON(&application)
	if err != nil {
		log.Default()
		return err
	}
	errSave := service.SaveApplication(&application)
	return errSave
}

func UpdateApplication(ctx *gin.Context) error {
	var param entity.UpdateApplicationParam
	err := ctx.ShouldBindJSON(&param)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}

	updateApplication := &entity.UpdateApplicationParam{
		Position: param.Position,
	}

	errUpdate := service.UpdateApplication(id, updateApplication)
	return errUpdate
}

func DeleteApplication(ctx *gin.Context) error {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}
	errDelete := service.DeleteApplication(id)
	return errDelete
}

func GetAllApplications(ctx *gin.Context) ([]entity.Application, error) {
	applications, err := service.FindAllApplications()
	if err != nil {
		return nil, err
	}
	return applications, nil
}

func GetApplicationById(ctx *gin.Context) (*entity.Application, error) {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	application, err := service.FindApplicationById(id)
	if err != nil {
		return nil, err
	}
	return application, nil
}
