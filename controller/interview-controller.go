package controller

import (
	"example/user/hello/entity"
	"example/user/hello/service"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
)

func AddInterview(ctx *gin.Context) error {
	var interview entity.Interview
	err := ctx.ShouldBindJSON(&interview)
	if err != nil {
		log.Default()
		return err
	}
	errSave := service.SaveInterview(&interview)
	return errSave
}

func UpdateInterview(ctx *gin.Context) error {
	var param entity.UpdateInterviewParam
	err := ctx.ShouldBindJSON(&param)
	if err != nil {
		return err
	}

	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}

	updateInterview := &entity.UpdateInterviewParam{
		DateTime:      param.DateTime,
		InterviewerID: param.InterviewerID,
	}

	errUpdate := service.UpdateInterview(id, updateInterview)
	return errUpdate
}

func DeleteInterviews(ctx *gin.Context) error {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	if err != nil {
		return err
	}
	errDelete := service.DeleteInterviews(id)
	return errDelete
}

func GetAllInterviews(ctx *gin.Context) ([]entity.Interview, error) {
	interviews, err := service.FindAllInterviews()
	if err != nil {
		return nil, err
	}
	return interviews, nil
}

func GetInterviewById(ctx *gin.Context) (*entity.Interview, error) {
	id, err := strconv.ParseUint(ctx.Param("id"), 64, 0)
	interview, err := service.FindInterviewById(id)
	if err != nil {
		return nil, err
	}
	return interview, nil
}
