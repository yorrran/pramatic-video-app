package controller

import (
	"example/user/hello/entity"
	"example/user/hello/middlewares"
	"example/user/hello/service"

	"github.com/gin-gonic/gin"
)

func Register(context *gin.Context) error {
	var input entity.AuthenticationInput

	if err := context.ShouldBindJSON(&input); err != nil {
		return err
	}

	user := &entity.User{
		Username: input.Username,
		Password: input.Password,
	}

	err := service.BeforeSave(user)

	if err != nil {
		return err
	}

	err1 := service.SaveUser(user)

	if err1 != nil {
		return err1
	}
	return nil
}

func Login(context *gin.Context) (*string, error) {
	var input entity.AuthenticationInput

	if err := context.ShouldBindJSON(&input); err != nil {
		return nil, err
	}

	user, err := service.FindUserByUsername(input.Username)

	if err != nil {
		return nil, err
	}

	err = service.ValidatePassword(user, input.Password)

	if err != nil {
		return nil, err
	}

	jwt, err := middlewares.GenerateJWT(*user)
	if err != nil {
		return nil, err
	}
	return &jwt, nil
}
