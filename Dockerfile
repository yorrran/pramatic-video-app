FROM golang:latest
LABEL maintainer="yoran <PangYaran@hotmail.sg>"
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
ENV PORT 3333
RUN go build -o /gohello
RUN find . -name "*.go" -type f -delete
EXPOSE $PORT
ENTRYPOINT ["/gohello"]
