package entity

import (
	"time"

	"gorm.io/gorm"
)

type Interview struct {
	gorm.Model
	ApplicationID string    `json:"applicationId"`
	DateTime      time.Time `json:"datetime"`
	InterviewerID string    `json:"interviewerId"`
}

type UpdateInterviewParam struct {
	DateTime      *time.Time `json:"datetime"`
	InterviewerID *string    `json:"interviewerId"`
}
