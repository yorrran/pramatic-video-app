package entity

import (
	database "example/user/hello/repository"
	"fmt"
	"time"
)

type Person struct {
	ID        uint64 `gorm:"primary_key;auto_increment" json:":id"`
	FirstName string `json:"firstname" binding:"required" gorm:"type:varchar(32)"`
	LastName  string `json:"lastname" binding:"required" gorm:"type:varchar(32)"`
	Age       int8   `json:"age" binding:"gte=1,lte=130"`
	Email     string `json:"email" validate:"required,email" gorm:"type:varchar(256)"`
}

type Video struct {
	ID          uint64    `gorm:"primary_key;auto_increment" json:":id"`
	Title       string    `json:"title" binding:"min=2,max=100" gorm:"type:varchar(100)"`
	Description string    `json:"description" binding:"max=100" gorm:"type:varchar(100)"`
	URL         string    `json:"url" binding:"required,url" gorm:"type:varchar(256)"`
	CreatedAt   time.Time `json:"-" gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt   time.Time `json:"-" gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

type UpdateVideoParam struct {
	ID          uint64  `gorm:"primary_key;auto_increment" json:":id"`
	Title       *string `json:"title" binding:"min=2,max=100" gorm:"type:varchar(100)"`
	Description *string `json:"description" binding:"max=100" gorm:"type:varchar(100)"`
	URL         *string `json:"url" binding:"required,url" gorm:"type:varchar(256)"`
}

func SaveVideo(video *Video) (*Video, error) {
	fmt.Println(video)
	err := database.Database.Table("videos").Create(&video).Error
	if err != nil {
		return nil, err
	}
	return video, nil
}

func UpdateVideo(param *UpdateVideoParam) (*Video, error) {
	video := &Video{}
	database.Database.Table("videos").Where("ID = ?", param.ID).First(&video)

	if param.Title != nil {
		video.Title = *param.Title
	}
	if param.Description != nil {
		video.Description = *param.Description
	}
	if param.URL != nil {
		video.URL = *param.URL
	}

	err := database.Database.Save(&video).Error
	if err != nil {
		return nil, err
	}
	return video, nil
}

func DeleteVideo(id uint64) (*Video, error) {
	video := &Video{}
	err := database.Database.Table("videos").Where("ID = ?", id).Delete(&video).Error
	if err != nil {
		return nil, err
	}
	return video, nil
}

func FindAllVideos() ([]Video, error) {
	var videos []Video
	err := database.Database.Table("videos").Find(&videos).Error
	if err != nil {
		return nil, err
	}
	fmt.Println(videos)
	return videos, nil
}
