package entity

import (
	"time"
)

type Application struct {
	ApplicationID uint64 `gorm:"primary_key;auto_increment" json:"applicationId"`
	CandidateID   string `json:"candidateId"`
	Position      string `json:"position"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

type UpdateApplicationParam struct {
	Position *string `json:"position"`
}
