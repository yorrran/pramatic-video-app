package entity

import (
	"time"
)

type User struct {
	UserID    uint64 `gorm:"primary_key;auto_increment" json:"userId"`
	Username  string `gorm:"size:255;not null;unique" json:"username"`
	Password  string `gorm:"size:255;not null;" json:"password"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
