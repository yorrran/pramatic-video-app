package entity

import "time"

type Candidate struct {
	CandidateID uint64 `gorm:"primary_key;auto_increment" json:"candiateId"`
	Username    string `gorm:"size:255;not null; json:"username"`
	Email       string `gorm:"size:255; json:"email"`
	Phone       string `gorm:"size:255; json:"phone"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type UpdateCandidateParam struct {
	Username *string `gorm:"size:255;not null; json:"username"`
	Email    *string `gorm:"size:255; json:"email"`
	Phone    *string `gorm:"size:255; json:"phone"`
}
