package service

import (
	"example/user/hello/entity"
	"example/user/hello/repository"
)

func SaveCandidate(candidate *entity.Candidate) error {
	err := repository.Database.Table("candidates").Create(&candidate).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateCandidate(id uint64, param *entity.UpdateCandidateParam) error {
	candidate := &entity.UpdateCandidateParam{}
	repository.Database.Table("candidates").Where("ID = ?", id).First(&candidate)

	if param.Username != nil {
		candidate.Username = param.Username
	}
	if param.Email != nil {
		candidate.Email = param.Email
	}
	if param.Phone != nil {
		candidate.Phone = param.Phone
	}

	err := repository.Database.Table("candidates").Save(&candidate).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteCandidate(id uint64) error {
	candidate := &entity.Candidate{}
	err := repository.Database.Table("candidates").Where("ID = ?", id).Delete(&candidate).Error
	if err != nil {
		return err
	}
	return nil
}

func FindAllCandidates() ([]entity.Candidate, error) {
	var candidates []entity.Candidate
	err := repository.Database.Table("candidates").Find(&candidates).Error
	if err != nil {
		return nil, err
	}
	return candidates, nil
}

func FindCandidateById(id uint64) (*entity.Candidate, error) {
	candidate := &entity.Candidate{}
	err := repository.Database.Table("candidates").First(&candidate, 1).Error
	if err != nil {
		return nil, err
	}
	return candidate, nil
}
