package service

import (
	"example/user/hello/entity"
	"example/user/hello/repository"
	"html"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func SaveUser(user *entity.User) error {
	err := repository.Database.Create(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func BeforeSave(user *entity.User) error {
	passwordHash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(passwordHash)
	user.Username = html.EscapeString(strings.TrimSpace(user.Username))
	return nil
}

func ValidatePassword(user *entity.User, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
}

func FindUserByUsername(username string) (*entity.User, error) {
	var user *entity.User
	err := repository.Database.Where("username=?", username).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func FindUserById(id uint) (*entity.User, error) {
	var user *entity.User
	err := repository.Database.Preload("Entries").Where("ID=?", id).Find(&user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}
