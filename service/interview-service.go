package service

import (
	"example/user/hello/entity"
	"example/user/hello/repository"
)

func SaveInterview(interview *entity.Interview) error {
	err := repository.Database.Table("interviews").Create(&interview).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateInterview(id uint64, param *entity.UpdateInterviewParam) error {
	interview := &entity.UpdateInterviewParam{}
	repository.Database.Table("interviews").Where("ID = ?", id).First(&interview)

	if param.DateTime != nil {
		interview.DateTime = param.DateTime
	}
	if param.InterviewerID != nil {
		interview.InterviewerID = param.InterviewerID
	}

	err := repository.Database.Table("interviews").Save(&interview).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteInterviews(id uint64) error {
	interview := &entity.Interview{}
	err := repository.Database.Table("interviews").Where("ID = ?", id).Delete(&interview).Error
	if err != nil {
		return err
	}
	return nil
}

func FindAllInterviews() ([]entity.Interview, error) {
	var interviews []entity.Interview
	err := repository.Database.Table("interviews").Find(&interviews).Error
	if err != nil {
		return nil, err
	}
	return interviews, nil
}

func FindInterviewById(id uint64) (*entity.Interview, error) {
	interview := &entity.Interview{}
	err := repository.Database.Table("interviews").First(&interview, 1).Error
	if err != nil {
		return nil, err
	}
	return interview, nil
}
