package service

import (
	"example/user/hello/entity"
	"example/user/hello/repository"
)

func SaveApplication(application *entity.Application) error {
	err := repository.Database.Table("applications").Create(&application).Error
	if err != nil {
		return err
	}
	return nil
}

func UpdateApplication(id uint64, param *entity.UpdateApplicationParam) error {
	application := &entity.UpdateApplicationParam{}
	repository.Database.Table("applications").Where("ID = ?", id).First(&application)

	if param.Position != nil {
		application.Position = param.Position
	}

	err := repository.Database.Table("applications").Save(&application).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteApplication(id uint64) error {
	application := &entity.Candidate{}
	err := repository.Database.Table("applications").Where("ID = ?", id).Delete(&application).Error
	if err != nil {
		return err
	}
	return nil
}

func FindAllApplications() ([]entity.Application, error) {
	var applications []entity.Application
	err := repository.Database.Table("applications").Find(&applications).Error
	if err != nil {
		return nil, err
	}
	return applications, nil
}

func FindApplicationById(id uint64) (*entity.Application, error) {
	application := &entity.Application{}
	err := repository.Database.Table("applications").First(&application, 1).Error
	if err != nil {
		return nil, err
	}
	return application, nil
}
